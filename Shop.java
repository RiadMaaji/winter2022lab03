import java.util.Scanner;
public class Shop {
  public static void main(String[] args) {
	  Scanner s = new Scanner(System.in);
	  Clothes[] products = new Clothes[4];
	  for (int i = 0; i < products.length; i++) {
		  products[i] = new Clothes();
		  System.out.println("Set the type of product #" + (i+1));
		  products[i].type = s.next();
		  System.out.println("Set the color of product #" + (i+1));
		  products[i].color = s.next();
		  System.out.println("Set the price of product #" + (i+1));
		  products[i].price = s.nextInt();
	  }
	  System.out.println("Type of the last product: " + products[3].type);
	  System.out.println("Color of the last product: " + products[3].color);
	  System.out.println("Price of the last product: " + products[3].price);
	  products[3].colorChoice();
  }
}